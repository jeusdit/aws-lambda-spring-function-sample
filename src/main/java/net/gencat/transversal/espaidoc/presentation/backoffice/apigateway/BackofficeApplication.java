package net.gencat.transversal.espaidoc.presentation.backoffice.apigateway;

import org.springframework.boot.SpringApplication;

public class BackofficeApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackofficeApplication.class, args);
	}

}
