package net.gencat.transversal.espaidoc.presentation.backoffice.apigateway.document.pushed;

import java.util.function.Function;

import org.springframework.stereotype.Component;

import com.amazonaws.services.lambda.runtime.events.models.s3.S3EventNotification;
import com.amazonaws.services.lambda.runtime.events.models.s3.S3EventNotification.S3Entity;
import com.amazonaws.services.lambda.runtime.events.models.s3.S3EventNotification.S3EventNotificationRecord;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@RequiredArgsConstructor
@Slf4j
public class PushedDocumentLambdaConsumer implements Function<S3EventNotification, String> {

  @Override
  public String apply(S3EventNotification s3EventNotification) {
    log.info("************************************************* RECEIVED TOJSON()");
    log.info("object class: {}", s3EventNotification.getClass().toString());
    log.info(s3EventNotification.toString());
    log.info("*************************************************");

    log.info("processing event");
    if (s3EventNotification != null && s3EventNotification.getRecords() != null) {
      log.info("Number of records: {}", s3EventNotification.getRecords().size());
      for (S3EventNotificationRecord s3EventNotificationRecord : s3EventNotification.getRecords()) {
        log.info("event name: {}", s3EventNotificationRecord.getEventName());
        log.info("event source: {}", s3EventNotificationRecord.getEventSource());

        S3Entity s3Entity = s3EventNotificationRecord.getS3();
        if (null != s3Entity) {
          log.info("s3Entity bucket name: {}", s3Entity.getBucket().getName());
          log.info("s3Entity object name: {}", s3Entity.getObject().getKey());
        }
      }
    }

    log.info("======================");
    return "Ok";
  }

}

